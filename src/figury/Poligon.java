package figury;

import java.awt.Graphics2D;
import java.awt.Polygon;
import java.awt.geom.AffineTransform;
import java.awt.geom.Area;

public class Poligon extends Figura{

	public Poligon(Graphics2D buf, int del, int w, int h) {
		super(buf, del, w, h);
		
		int x[] = {1, 4, 5, 1, -3, -7};
		int y[] = {40, 30, 10, -10, -7, 20};
		
		shape = new Polygon(x, y, 6);
		aft = new AffineTransform();
		area = new Area(shape);
		
		
	}

}
