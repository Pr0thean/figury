package figury;

import java.awt.EventQueue;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.JButton;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.geom.AffineTransform;
import java.awt.event.ActionEvent;

public class AnimatorApp extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					final AnimatorApp frame = new AnimatorApp();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	/**
	 * Create the frame.
	 * @param delay 
	 */
	public AnimatorApp() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
		int ww = 450, wh = 300;
		setBounds((screen.width-ww)/2, (screen.height-wh)/2, ww, wh);
		
		contentPane = new JPanel();
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(15, 15));

		AnimPanel kanwa = new AnimPanel();
		//kanwa.setBounds(10, 11, 422, 219);
		
		// Resize dzia�a ale znikaj� przyciski, trzeba odkomentowac gettery
		kanwa.addComponentListener(new ComponentAdapter() {
			public void componentResized(ComponentEvent ev)
		    {
				AnimPanel.width = getWidth();
				AnimPanel.height = getHeight();

				kanwa.image = createImage(AnimPanel.width, AnimPanel.height);
				AnimPanel.buffer = (Graphics2D) kanwa.image.getGraphics();
				AnimPanel.buffer.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
				kanwa.device = (Graphics2D) getGraphics();
				kanwa.device.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
				AnimPanel.buffer.setBackground(Color.WHITE);
				for(Figura element : kanwa.figures)
				{
					element.changeParams(AnimPanel.buffer, AnimPanel.width, AnimPanel.height);
				}
		    }  
		});
		SwingUtilities.invokeLater(new Runnable() {
			
			@Override
			public void run() {
				kanwa.initialize();
			}
		});
		contentPane.add(kanwa, BorderLayout.CENTER);
		
		JButton btnAdd = new JButton("Add");
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				kanwa.addFig();
			}
		});
		//btnAdd.setBounds(10, 239, 80, 23);
		contentPane.add(btnAdd, BorderLayout.BEFORE_FIRST_LINE);
		
		JButton btnAnimate = new JButton("Animate");
		btnAnimate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				kanwa.animate();
			}
		});
		//btnAnimate.setBounds(100, 239, 80, 23);
		contentPane.add(btnAnimate, BorderLayout.AFTER_LAST_LINE);

	}
}
